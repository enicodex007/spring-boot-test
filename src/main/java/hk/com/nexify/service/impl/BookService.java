package hk.com.nexify.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import hk.com.nexify.dao.test.impl.BookDaoImp;
import hk.com.nexify.entity.test.Book;

@Service
public class BookService {

	@Autowired
	private BookDaoImp bookDaoImp;
	
	@Transactional(readOnly = true)
	public List<Book> getAll(){
		return bookDaoImp.findAll();
	}
	
	@Transactional(readOnly = true)
	public Book findById(Long id){
		return bookDaoImp.find(id);
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean save(Book book){
		return bookDaoImp.save(book);
	}
	
	@Transactional
	public boolean delete(Long id){
		return bookDaoImp.removeById(id);
	}
	
	
	
}
