package hk.com.nexify.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import hk.com.nexify.dao.test.impl.DepartmentDaoImp;
import hk.com.nexify.entity.test.Department;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentDaoImp departmentDaoImp;
	
	@Transactional(readOnly = true)
	public List<Department> getAll(){
		return departmentDaoImp.findAll();
	}
	
	@Transactional(readOnly = true)
	public Department findById(Long id){
		return departmentDaoImp.find(id);
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean save(Department book){
		return departmentDaoImp.save(book);
	}
	
	@Transactional
	public boolean delete(Long id){
		return departmentDaoImp.removeById(id);
	}
	
	
	
}
