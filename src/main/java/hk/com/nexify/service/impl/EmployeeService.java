package hk.com.nexify.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import hk.com.nexify.dao.test.impl.EmployeeDaoImp;
import hk.com.nexify.entity.test.Employee;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDaoImp employeeDaoImp;
	
	@Transactional(readOnly = true)
	public List<Employee> getAll(){
		return employeeDaoImp.findAll();
	}
	
	@Transactional(readOnly = true)
	public Employee findById(Long id){
		return employeeDaoImp.find(id);
	}
	
	@Transactional(isolation = Isolation.SERIALIZABLE)
	public boolean save(Employee employee){
		return employeeDaoImp.save(employee);
	}
	
	@Transactional
	public boolean delete(Long id){
		return employeeDaoImp.removeById(id);
	}
	
	
	
}
