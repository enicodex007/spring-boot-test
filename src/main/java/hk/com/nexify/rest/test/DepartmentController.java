package hk.com.nexify.rest.test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import hk.com.nexify.entity.test.Department;
import hk.com.nexify.service.impl.DepartmentService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/department")
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("department-list");
		modelAndView.addObject("departments", departmentService.getAll());
		return modelAndView;
	}
	
	@GetMapping(value = "/{id}")
	public @ResponseBody Department getDepartmentById(@PathVariable(value = "id", required = true) Long id) {
		return departmentService.findById(id);
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView modelAndView = new ModelAndView("department");
		modelAndView.addObject("department", new Department());
		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable(value = "id", required = true) Long id) {
		ModelAndView modelAndView = new ModelAndView("department");
		modelAndView.addObject("department", departmentService.findById(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable(value = "id", required = true) Long id) {
		boolean deleted = departmentService.delete(id);
		log.info("Department-{} has been deleted!", id, deleted);
		return new ResponseEntity<String>("Department-"+id+" has been deleted!",HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("department") Department department) {
		log.info("save-{}", department);
		boolean saved = departmentService.save(department);
		log.info("Department-{} has been saved - {}!", department, saved);
		return "redirect:/department/index";
	}
}