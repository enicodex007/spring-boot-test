package hk.com.nexify.rest.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import hk.com.nexify.entity.test.Book;
import hk.com.nexify.service.impl.BookService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/book")
public class BookController {

	@Autowired
	private BookService bookService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("book-list");
		modelAndView.addObject("books", bookService.getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView modelAndView = new ModelAndView("book");
		modelAndView.addObject("book", new Book());
		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable(value = "id", required = true) Long id) {
		ModelAndView modelAndView = new ModelAndView("book");
		modelAndView.addObject("book", bookService.findById(id));
		return modelAndView;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable(value = "id", required = true) Long id) {
		boolean deleted = bookService.delete(id);
		log.info("Book-{} has been deleted!", id, deleted);
		return new ResponseEntity<String>("Book-"+id+" has been deleted!",HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("book") Book book, RedirectAttributes attr) {
		boolean saved = bookService.save(book);
		log.info("Book-{} has been saved - {}!", book, saved);
		return "redirect:/book/index";
	}
}