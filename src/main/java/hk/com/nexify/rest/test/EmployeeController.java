package hk.com.nexify.rest.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import hk.com.nexify.entity.test.Employee;
import hk.com.nexify.service.impl.DepartmentService;
import hk.com.nexify.service.impl.EmployeeService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("employee-list");
		modelAndView.addObject("employees", employeeService.getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView modelAndView = new ModelAndView("employee");
		modelAndView.addObject("employee", new Employee());
		modelAndView.addObject("departments", departmentService.getAll());
		return modelAndView;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable(value = "id", required = true) Long id) {
		ModelAndView modelAndView = new ModelAndView("employee");
		modelAndView.addObject("employee", employeeService.findById(id));
		modelAndView.addObject("departments", departmentService.getAll());
		return modelAndView;
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable(value = "id", required = true) Long id) {
		boolean deleted = employeeService.delete(id);
		log.info("employee-{} has been deleted!", id, deleted);
		return new ResponseEntity<String>("employee-"+id+" has been deleted!",HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("employee") Employee employee) {
		log.info("save-{}", employee);
		boolean saved = employeeService.save(employee);
		log.info("Employee-{} has been saved - {}!", employee, saved);
		return "redirect:/employee/index";
	}
}