package hk.com.nexify.dao.test.impl;

import org.springframework.stereotype.Repository;

import hk.com.nexify.dao.cmn.impl.GenericBaseDaoImpl;
import hk.com.nexify.entity.test.Department;

@Repository
public class DepartmentDaoImp extends GenericBaseDaoImpl<Department, Long> {

}
