package hk.com.nexify.dao.test.impl;

// import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
// import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import hk.com.nexify.dao.cmn.impl.GenericBaseDaoImpl;
import hk.com.nexify.entity.test.Book;


@Repository
public class BookDaoImp extends GenericBaseDaoImpl<Book, Long> {

}
