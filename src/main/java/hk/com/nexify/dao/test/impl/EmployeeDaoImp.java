package hk.com.nexify.dao.test.impl;

import org.springframework.stereotype.Repository;

import hk.com.nexify.dao.cmn.impl.GenericBaseDaoImpl;
import hk.com.nexify.entity.test.Department;
import hk.com.nexify.entity.test.Employee;

@Repository
public class EmployeeDaoImp extends GenericBaseDaoImpl<Employee, Long> {

}
