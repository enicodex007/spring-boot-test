/**
 * Main.js
 */
 
 // delete entity from the database
function deleteEntity(deleteURL) {
	if(!confirm("Are you sure you want to delete this?")){
      return false;  
    }
	
    $.ajax({

        type: "DELETE",
        url: deleteURL,

        success: function () {
            window.location.reload();
            // $('#myTableRow').remove();
        },

        failure: function (errMsg) {
            console.log(errMsg.toString())
        }
    });
}