CREATE TABLE book (
    id integer NOT NULL PRIMARY KEY,
    author VARCHAR(100) NOT NULL,
    title VARCHAR(200) NOT NULL
);

CREATE TABLE department (
    id integer NOT NULL PRIMARY KEY,
    department_name VARCHAR(200) NOT NULL
);

CREATE TABLE employee (
    id integer NOT NULL PRIMARY KEY,
    employee_name VARCHAR(255) NOT NULL,
    salary decimal,
    department_id integer not null
);