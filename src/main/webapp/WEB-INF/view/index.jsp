<%@ include file="common/header.jspf"%>

<h2>Index Page</h2>
<hr />
<div class="container">
	<div class="panel-body">
		<table border="1" style="width: 70%">
			<thead>
				<tr>
					<th>Link</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<a type="button" class="btn btn-success" href="/book/index">Book</a>
					</td>
				</tr>
				<tr>
					<td>
						<a type="button" class="btn btn-success" href="/department/index">Department</a>
					</td>
				</tr>
				<tr>					
					<td>
						<a type="button" class="btn btn-success" href="/employee/index">Employee</a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<%@ include file="common/footer.jspf"%>