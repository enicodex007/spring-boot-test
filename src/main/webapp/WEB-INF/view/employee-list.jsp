<%@ include file="common/header.jspf"%>

<h2>Employee List</h2>
<hr />
<div class="container">
	<div class="panel-body">
		<table border="1" style="width: 70%">
			<thead>
				<tr>
					<th>Name</th>
					<th>Salary</th>
					<th>Department Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="books">
				<c:forEach items="${employees}" var="employee">
					<tr>
		               <td>${employee.employeeName}</td>
		               <td>${employee.salary}</td>
		               <td>${employee.department.departmentName}</td>
		               <td>
		               	<a type="button" class="btn btn-success" href="/employee/update/${employee.id}">Edit</a> &nbsp;
		               	<button class="btn btn-warning" onclick="deleteEntity('${pageContext.request.contextPath}/employee/delete/${employee.id}')">Delete</button>
		               </td>
		         	</tr>
		        </c:forEach>
			</tbody>
		</table>
	</div>
	
	<div>
	  <a type="button" class="btn btn-primary btn-md" href="/employee/new">New</a>
	</div>
</div>
<%@ include file="common/footer.jspf"%>