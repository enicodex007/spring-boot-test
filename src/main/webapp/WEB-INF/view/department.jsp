<%@ include file="common/header.jspf"%>

<h2>Department</h2>
<hr />
<div class="container">
	<form:form action = "${pageContext.request.contextPath}/department/save" modelAttribute="department" method="POST">
		Name: <form:input path="departmentName"/>
		<br/>
		<form:hidden path = "id"/>
		<input type = "submit" value="Save"/>
	</form:form>
</div>
<%@ include file="common/footer.jspf"%>