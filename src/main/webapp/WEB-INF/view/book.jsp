<%@ include file="common/header.jspf"%>

<h2>Book</h2>
<hr />
<div class="container">
	<form:form action = "${pageContext.request.contextPath}/book/save" modelAttribute="book" method="POST">
		Title: <form:input path="title"/>
		<br/>
		Author: <form:input path="author"/>
		<br/>
		<form:hidden path = "id"/>
		<input type = "submit" value="Save"/>
	</form:form>
</div>
<%@ include file="common/footer.jspf"%>