<%@ include file="common/header.jspf"%>

<h2>List Books</h2>
<hr />
<div class="container">
	<div class="panel-body">
		<table border="1" style="width: 70%">
			<thead>
				<tr>
					<th>Title</th>
					<th>Author</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="books">
				<c:forEach items="${books}" var="book">
					<tr>
		               <td>${book.title}</td>
		               <td>${book.author}</td>
		               <td>
		               	<a type="button" class="btn btn-success" href="/book/update/${book.id}">Edit</a> &nbsp;
		               	<button class="btn btn-warning" onclick="deleteEntity('${pageContext.request.contextPath}/book/delete/${book.id}')">Delete</button>
		               </td>
		         	</tr>
		        </c:forEach>
			</tbody>
		</table>
	</div>
	
	<div>
	  <a type="button" class="btn btn-primary btn-md" href="/book/new">New</a>
	</div>
</div>
<%@ include file="common/footer.jspf"%>