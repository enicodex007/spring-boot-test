<%@ include file="common/header.jspf"%>

<h2>Employee</h2>
<hr />
<div class="container">
	<form:form action = "${pageContext.request.contextPath}/employee/save" modelAttribute="employee">
		Name: <form:input path="employeeName"/>
		<br/>
		Salary: <form:input path="salary"/>
		<br/>
		Department: 
		<form:select id="departmentId" path="department.id" cssClass="form-control">
		     <form:option value="0" label="n/a" />
		     <form:options items="${departments}" itemValue="id" itemLabel="departmentName" />
	    </form:select>
		<br/>
		<form:hidden path = "id"/>
		<input type = "submit" value="Save"/>
	</form:form>
</div>
<%@ include file="common/footer.jspf"%>