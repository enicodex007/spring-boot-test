<%@ include file="common/header.jspf"%>

<h2>Department List</h2>
<hr />
<div class="container">
	<div class="panel-body">
		<table border="1" style="width: 70%">
			<thead>
				<tr>
					<th>Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="books">
				<c:forEach items="${departments}" var="department">
					<tr>
		               <td>${department.departmentName}</td>
		               <td>
		               	<a type="button" class="btn btn-success" href="/department/update/${department.id}">Edit</a> &nbsp;
		               	<button class="btn btn-warning" onclick="deleteEntity('${pageContext.request.contextPath}/department/delete/${department.id}')">Delete</button>
		               </td>
		         	</tr>
		        </c:forEach>
			</tbody>
		</table>
	</div>
	
	<div>
	  <a type="button" class="btn btn-primary btn-md" href="/department/new">New</a>
	</div>
</div>
<%@ include file="common/footer.jspf"%>